from __future__ import annotations

from pathlib import Path

import pytest
from pydantic import UUID4, BaseModel, Field

from alloniaconfigs import Configs

# Define your own alias for union types


class MySchema(BaseModel):
    ID: str | None = Field(None, min_length=16, max_length=16)
    ID_MANDATORY: UUID4 = Field()
    SOME_STRING: str | None = Field(None)


@pytest.fixture(autouse=True)
def _setschema():
    Configs.reset()
    Configs.schema = MySchema


@pytest.fixture
def make_envfile():
    value = "Hello"
    with Path(".env").open("w") as envfile:
        envfile.write(f"SOME_STRING={value}")
    yield value
    Path(".env").unlink()
