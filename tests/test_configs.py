import os

import pytest
from expects import be_false, be_none, contain, equal, expect
from pydantic import ValidationError

from alloniaconfigs import Configs

mandatory_id = "b3568fd7-20c5-4d32-a488-9c34e49aa23d"


def test_configs():
    with pytest.raises(ValidationError) as error:
        _ = Configs.instance
    expect(str(error.value)).to(contain("ID_MANDATORY"))
    expect(str(error.value)).to(contain("Field required"))
    configs = Configs(ID_MANDATORY=mandatory_id)
    expect(configs.instance.ID).to(equal(os.environ["ID"]))
    expect(str(configs.instance.ID_MANDATORY)).to(equal(mandatory_id))
    expect(Configs.instance.ID).to(equal(os.environ["ID"]))
    expect(str(Configs.instance.ID_MANDATORY)).to(equal(mandatory_id))
    expect(hasattr(Configs.instance, "DUMMY_VAR")).to(be_false)
    expect(hasattr(Configs.instance, "DUMMY_VAR")).to(be_false)
    expect(configs.instance.SOME_STRING).to(be_none)
    expect(Configs.instance.SOME_STRING).to(be_none)


def test_configs_with_envfile(make_envfile):
    with pytest.raises(ValidationError) as error:
        _ = Configs.instance
    expect(str(error.value)).to(contain("ID_MANDATORY"))
    expect(str(error.value)).to(contain("Field required"))
    configs = Configs(ID_MANDATORY=mandatory_id)
    expect(configs.ID).to(equal(os.environ["ID"]))
    expect(str(configs.ID_MANDATORY)).to(equal(mandatory_id))
    expect(Configs.instance.ID).to(equal(os.environ["ID"]))
    expect(str(Configs.instance.ID_MANDATORY)).to(equal(mandatory_id))
    expect(hasattr(configs, "DUMMY_VAR")).to(be_false)
    expect(hasattr(Configs.instance, "DUMMY_VAR")).to(be_false)
    expect(configs.SOME_STRING).to(equal(make_envfile))
    expect(Configs.instance.SOME_STRING).to(equal(make_envfile))


def test_wrong_schema():
    Configs.schema = int
    with pytest.raises(TypeError) as error:
        _ = Configs.instance
    expect(str(error.value)).to(contain("The 'schema' argument"))


def test_set_instance():
    with pytest.raises(ValueError, match="manually"):
        Configs(ID_MANDATORY=mandatory_id).instance = 0
