## [1.1.2](https://gitlab.com/aleia-team/public/alloniaconfigs/compare/1.1.1...1.1.2) (2025-01-14)


### Bug Fixes

* **configs:** support multiple configs instances ([7ed61a3](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/7ed61a36adac2076849d2175ac1537a6c226b53d))

## [1.1.1](https://gitlab.com/aleia-team/public/alloniaconfigs/compare/1.1.0...1.1.1) (2024-05-15)


### Bug Fixes

* pages ([52d0b00](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/52d0b00b6a911f9c5e4a9c0d8c72b58c5fe62f46))

# [1.1.0](https://gitlab.com/aleia-team/public/alloniaconfigs/compare/1.0.6...1.1.0) (2024-05-06)


### Features

* supports python 3.9-10-11-12 ([1d479ea](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/1d479ea0df738b943c0c69fd3fdfa209ed67340f))

## [1.0.6](https://gitlab.com/aleia-team/public/alloniaconfigs/compare/1.0.5...1.0.6) (2024-04-17)


### Bug Fixes

* ci ([c2d7192](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/c2d71925751a3e9be22b1d4ad54ee3e68fee812d))

## [1.0.5](https://gitlab.com/aleia-team/public/alloniaconfigs/compare/1.0.4...1.0.5) (2024-04-17)


### Bug Fixes

* ci ([edc0a6f](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/edc0a6fa0f60fb63c03de768d448d75570c3fdf8))

## [1.0.4](https://gitlab.com/aleia-team/public/alloniaconfigs/compare/1.0.3...1.0.4) (2024-04-17)


### Bug Fixes

* ci ([ddc0bd2](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/ddc0bd281e5c081dd7f59238c8c96529655c5fcd))

## [1.0.3](https://gitlab.com/aleia-team/public/alloniaconfigs/compare/1.0.2...1.0.3) (2024-04-17)


### Bug Fixes

* ci ([3fdfb18](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/3fdfb18cd473584e8fdfc1df291adee0390f4175))
* ci ([5e66605](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/5e666051fdfafcc0680089bdd34e6559d4f93281))
* ci ([a5b8874](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/a5b88744f3f291bb4f1faea1f0c7814e1bb83b2c))

## [1.0.2](https://gitlab.com/aleia-team/public/alloniaconfigs/compare/1.0.1...1.0.2) (2024-04-17)


### Bug Fixes

* compile doc ([139afb9](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/139afb935e148edecaf88e08d689db554a40c931))
* compile doc ([840e937](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/840e937139d9c5f52c8453cd95c4ef122246b032))
* compile doc ([8f23cf3](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/8f23cf3d521faaeae487ee221e0f00ec2e545868))
* compile doc ([56cdf4a](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/56cdf4abb10c59c743143ce47262acf03510a007))

## [1.0.1](https://gitlab.com/aleia-team/public/alloniaconfigs/compare/1.0.0...1.0.1) (2024-04-16)


### Bug Fixes

* compile doc ([2ff4bfb](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/2ff4bfb24c40d3e4e2dae044a54325a0f4a20d47))

# 1.0.0 (2024-04-16)


### Bug Fixes

* rename project ([23d4c46](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/23d4c46e0c50e8b3b9d48ec751a883911ea9c6c4))
* rename project ([93c44e7](https://gitlab.com/aleia-team/public/alloniaconfigs/commit/93c44e7ef6d4a16c339c5cbe2df96408cbbadb45))
