import os.path
import sys
from datetime import datetime

sys.path.insert(0, os.path.abspath("../../"))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "alloniaconfigs"
copyright = f"{datetime.now().year}, AllOnIA"
author = "AllOnIA"
source_suffix = [".md", ".rst"]
release = "0.1.0"
# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

rst_prolog = """
.. role:: inlinepython(code)
   :language: python
"""

extensions = [
    "sphinx.ext.duration",
    "sphinx.ext.doctest",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
    "sphinx_mdinclude",
    "sphinx_copybutton",
]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "pydata_sphinx_theme"
html_theme_options = {
    # "navbar_start": ["navbar-logo", "version"],
    "navigation_with_keys": True,
}
exclude_patterns = ["prolog.rst", ".*", "docs"]
html_static_path = ["_static"]
html_logo = "allonia-logo.svg"

# html_css_files = [
#     'css/custom.css'
# ]

html_context = {"default_mode": "light"}
html_sidebars = {
    "**": [
        "search-field.html",
        "globaltoc.html",
        "relations.html",
        "sourcelink.html",
    ]
}

intersphinx_mapping = {
    "pydantic": ("https://docs.pydantic.dev/latest/", None),
}

napoleon_google_docstring = True
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = True
napoleon_use_ivar = True
napoleon_use_param = True
napoleon_use_rtype = True
napoleon_preprocess_types = False
napoleon_type_aliases = True
napoleon_attr_annotations = True
