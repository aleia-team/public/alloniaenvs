############################################
 Welcome to AllOnIAConfigs's documentation !
############################################

-----------------------------
  Latest release: |release|
-----------------------------

.. toctree::
   :hidden:
   :caption: Table of Contents:

   docstrings

.. mdinclude:: ../../README.md