###########################
 AllOnIAConfigs Docstrings
###########################

***************
 Configs Class
***************

.. autoclass:: alloniaconfigs.configs.Configs
   :members:

.. autoclass:: alloniaconfigs.configs._ConfigsInstance
   :members:
